﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Imaginamos.CubeSummation
{
    public class Program
    {
        static void Main(string[] args)
        {

            ProcessData();

        }

        public static void ProcessData()
        {
            try
            {
                Console.WriteLine("-- Prueba Tecnica Imaginamos -- [Juan David Pineda]");
                Console.WriteLine("Ingrese T y Finalice con la tecla Enter");
                int T = int.Parse(Console.ReadLine());
                List<int> results = new List<int>();
                for (int i = 0; i < T; i++)
                {
                    Console.WriteLine("Ingrese N [Espacio] M y Finalice con la tecla Enter");
                    string input = Console.ReadLine();
                    int N = int.Parse(input.Split(' ')[0]);
                    int M = int.Parse(input.Split(' ')[1]);

                    int[][][] matrix = new int[N][][];

                    for (int j = 0; j < N; j++)
                    {
                        matrix[j] = new int[N][];

                        for (int k = 0; k < N; k++)
                        {
                            matrix[j][k] = new int[N];
                        }
                    }

                    for (int j = 0; j < matrix.Length; j++)
                    {
                        for (int k = 0; k < matrix.Length; k++)
                        {
                            for (int l = 0; l < matrix.Length; l++)
                            {
                                matrix[j][k][l] = 0;
                            }
                        }
                    }
                    Console.WriteLine("Ingrese QUERY  ó  UPDATE  seguido del formato siguiente:");
                    Console.WriteLine("UPDATE [Posicion1] [Posicion2] [Posicion3] [ValorActualizar]");
                    Console.WriteLine("QUERY [Posicion1] [Posicion2] [Posicion3] [Posicion4] [Posicion5] [Posicion6]");

                    for (int j = 0; j < M; j++)
                    {

                        Console.WriteLine("Ingrese QUERY  ó  UPDATE # " + (j + 1));
                        var type = Console.ReadLine().Split(' ');
                        switch (type[0])
                        {
                            case "QUERY":
                                int total = 0;

                                for (int j1 = int.Parse(type[1]) - 1; j1 <= int.Parse(type[4]) - 1; j1++)
                                {
                                    for (int k = int.Parse(type[2]) - 1; k <= int.Parse(type[5]) - 1; k++)
                                    {
                                        for (int l = int.Parse(type[3]) - 1; l <= int.Parse(type[6]) - 1; l++)
                                        {
                                            total += matrix[j1][k][l];
                                        }
                                    }
                                }
                                results.Add(total);
                                break;
                            case "UPDATE":
                                matrix[int.Parse(type[1]) - 1][int.Parse(type[2]) - 1][int.Parse(type[3]) - 1] = int.Parse(type[4]);
                                break;

                        }
                    }

                }
                Console.WriteLine("Resultado");
                results.ForEach(c => Console.WriteLine(c.ToString()));

                Console.ReadLine();
            }
            catch (Exception)
            {

                Console.WriteLine("Ocurrió un error al momento de procesar la información");
                Console.ReadLine();
            }
        }
    }
}
